# NE2



## Introduccion

Este repositorio consta de cada una de las distintas ramas de avance del proyecto
**Es importante que cada uno de los avances sea implementado en su propia rama para evitar confusiones de codigo!!**

## Demostracion

Se encontrara en youtube el video con los avances más actualizados hasta el momento.
El ultimo link disponible es: https://youtu.be/3HgMIZaFT6E

## Sobre la visualización 

El codigo de color sobre los pies del jugador que manejamos actualmente es:
Rojo: el pie no se encuentra sobre ninguna zona de interes.
Amarillo: el pie se encuentra sobre una zona de interes de manera parcial.
Rojo: el pie se encuentra sobre una zona de interes de manera completa.

El codigo de color sobre las zonas de interes que manejamos actualmente es:
**Sin nada que presentar actualmente...**
