import cv2
import mediapipe as mp
import numpy as np
import cv2 as cv


'''
src_list = [[factorHeight* 19, factorWidth*362], [factorHeight*176, factorWidth*375], [factorHeight*135, factorWidth*277],
            [factorHeight*484, factorWidth*395], [factorHeight*641, factorWidth*409], [factorHeight*806, factorWidth*423],
            [factorHeight*730, factorWidth*318], [factorHeight*692, factorWidth*251], [factorHeight*677, factorWidth*228],
            [factorHeight*212, factorWidth*222], [factorHeight*239, factorWidth*199], [factorHeight*495, factorWidth*214],
            [factorHeight*494, factorWidth*300], [factorHeight*391, factorWidth*258]]

dst_list = [[101, 613], [203, 611], [102, 407],
            [408, 611], [510, 614], [611, 614],
            [614, 406], [613, 202], [613, 100],
            [101, 203], [104, 100], [410, 100],
            [407, 408], [306, 304]]
'''

src_list = [[8.5875706, 162.792848], [364.2937844, 190.224792], [305.9886998, 102.532512],
            [108.0225986, 89.491096], [223.2768356, 134.9112], [176.72316339999998, 116.02363199999999]]

dst_list = [[101, 613],[611, 614],[613, 100],
            [104, 100],[407, 408],[306, 304]]

def verificar_punto(pts1,pts2):
    contador1 = 0
    contador2 = 0
    detection1 = -1
    detection2 = -1
    for punto in pts1:
        x = punto[0]
        y = punto[1]
        if 101 < x and x < 201:
            if 103<y and y<205:
                contador1 +=1
                detection1 = 9
            elif 308<y and y<410:
                contador1 +=1
                detection1 = 6
            elif 514<y and y<615:
                contador1 +=1
                detection1 = 3
        elif 305<x and x <406:
            if 103<y and y<205:
                contador1 +=1
                detection1 = 8
            elif 308 < y and y <410:
                contador1 +=1
                detection1 = 5
            elif 514<y and y<615:
                contador1 +=1
                detection1 = 2
        elif 510<x and x<610:
            if 103<y and y<205:
                contador1 +=1
                detection1 = 7
            elif 308<y and y<410:
                contador1 +=1
                detection1 = 4
            elif 514<y and y<615:
                contador1 +=1
                detection1 = 1
    for punto in pts2:
        x = punto[0]
        y = punto[1]
        if 101 < x and x < 201:
            if 103<y and y<205:
                contador2 +=1
                detection2 = 9
            elif 308<y and y<410:
                contador2 +=1
                detection2 = 6
            elif 514<y and y<615:
                contador2 +=1
                detection2 = 3
        elif 305<x and x <406:
            if 103<y and y<205:
                contador2 +=1
                detection2 = 8
            elif 308 < y and y <410:
                contador2 +=1
                detection2 = 5
            elif 514<y and y<615:
                contador2 +=1
                detection2 = 2
        elif 510<x and x<610:
            if 103<y and y<205:
                contador2 +=1
                detection2 = 7
            elif 308<y and y<410:
                contador2 +=1
                detection2 = 4
            elif 514<y and y<615:
                contador2 +=1
                detection2 = 1
    return [contador1,contador2,detection1,detection2]

def homography_transformation(x_1, y_1,H):
    P = [x_1,y_1]
    P_final = []

    x= (H[0][0]*P[0]+H[0][1]*P[1]+H[0][2])/(H[2][0]*P[0]+H[2][1]*P[1]+H[2][2])
    y= (H[1][0]*P[0]+H[1][1]*P[1]+H[1][2])/(H[2][0]*P[0]+H[2][1]*P[1]+H[2][2])

    P_final.append(x)
    P_final.append(y)
    return(P_final)

def main_(src_list, dst_list,path):
    src = cv.imread('fabian.jpg', -1)
    src_copy = src.copy()
    
    dst = cv.imread('cancha.jpg', -1)
    dst_copy = dst.copy()

    src_pts = np.array(src_list).reshape(-1,1,2)
    dst_pts = np.array(dst_list).reshape(-1,1,2)
    H, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
    
    

    mp_drawing = mp.solutions.drawing_utils
    mp_pose = mp.solutions.pose

    cap = cv2.VideoCapture(path)

    with mp_pose.Pose(
        static_image_mode=False) as pose:
        while True:
            
            ret, frame = cap.read()
            if ret == False:
                break

            frame = cv2.flip(frame, 1)
            height, width, _ = frame.shape
            factorHeight,factorWidth = height/676,width/885,
            image_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            results = pose.process(image_rgb)
           
            if results.pose_landmarks is not None:
                pd_30 = [width*results.pose_landmarks.landmark[30].x,
                         height*results.pose_landmarks.landmark[30].y]

                pd_32= [width*results.pose_landmarks.landmark[32].x,
                           height*results.pose_landmarks.landmark[32].y]

                pi_29 = [width*results.pose_landmarks.landmark[29].x,
                         height*results.pose_landmarks.landmark[29].y]

                pi_31= [width*results.pose_landmarks.landmark[31].x,
                           height*results.pose_landmarks.landmark[31].y]

                pts_D = np.array([pd_30,pd_32],np.int32) # pie
                pts_I = np.array([pi_29,pi_31],np.int32) # pie

                pdH_30 = homography_transformation(pd_30[0], pd_30[1],H)
                pdH_32 = homography_transformation(pd_32[0], pd_32[1],H)

                piH_29 = homography_transformation(pi_29[0], pi_29[1],H)
                piH_31 = homography_transformation(pi_31[0], pi_31[1],H)


                pts_H_D = np.array([pdH_30,pdH_32],np.int32) # pie Homografiado
                pts_H_I = np.array([piH_29,piH_31],np.int32) # pie Homografiado

                stateFoot = verificar_punto(pts_H_D,pts_H_I)

                if(stateFoot[0]==2):
                    cv.polylines(frame,[pts_D],True,(0,255,0),14, cv.LINE_AA)
                elif(stateFoot[0]== 1):
                    cv.polylines(frame,[pts_D],True,(0,255,255),14, cv.LINE_AA)
                else:
                    cv.polylines(frame,[pts_D],True,(0,0,255),14, cv.LINE_AA)

                if(stateFoot[1]==2):
                    cv.polylines(frame,[pts_I],True,(0,255,0),14, cv.LINE_AA)
                elif(stateFoot[1]== 1):
                    cv.polylines(frame,[pts_I],True,(0,255,255),14, cv.LINE_AA)
                else:
                    cv.polylines(frame,[pts_I],True,(0,0,255),14, cv.LINE_AA)
                mp_drawing.draw_landmarks(
                    frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                    mp_drawing.DrawingSpec(color=(128, 0, 250), thickness=2, circle_radius=3),
                    mp_drawing.DrawingSpec(color=(255, 255, 255), thickness=2))
                
                frame = cv2.flip(frame, 1)
                #1
                p1=np.array([[factorHeight*117,factorWidth*362],[factorHeight*254,factorWidth*351],[factorHeight*244,factorWidth*411],  [factorHeight*86,factorWidth*421]],np.int32)
                cv.polylines(frame,[p1],True,(0,255,0) if stateFoot[2] == 1 and stateFoot[3] == 1 else (0,255,255),1, cv.LINE_AA)
                #2
                p2 = np.array([[factorHeight*392,factorWidth*341],[factorHeight*530,factorWidth*334],[factorHeight*558,factorWidth*387],[factorHeight*398, factorWidth*387 ]],np.int32)
                cv.polylines(frame,[p2],True,(0,255,0) if stateFoot[2] == 2 and stateFoot[3] == 2 else (0,255,255),1, cv.LINE_AA)
                #3
                p3 = np.array([[factorHeight*667,factorWidth*324],[factorHeight*794,factorWidth*313],[factorHeight*868,factorWidth*360],[factorHeight*714,factorWidth*369 ]],np.int32)
                cv.polylines(frame,[p3],True,(0,255,0) if stateFoot[2] == 3 and stateFoot[3] == 3 else (0,255,255),1, cv.LINE_AA)
                #4
                p4 = np.array([[factorHeight*175,factorWidth*285],[factorHeight*281,factorWidth*278],[factorHeight*270,factorWidth*310],[factorHeight*159,factorWidth*313]],np.int32)
                cv.polylines(frame,[p4],True,(0,255,0) if stateFoot[2] == 4 and stateFoot[3] == 4 else (0,255,255),1, cv.LINE_AA)
                #5
                p5 = np.array([[factorHeight*388,factorWidth*260],[factorHeight*492,factorWidth*254],[factorHeight*515,factorWidth*289],[factorHeight*393,factorWidth*297]],np.int32)
                cv.polylines(frame,[p5],True,(0,255,0) if stateFoot[2] == 5 and stateFoot[3] == 5 else (0,255,255),1, cv.LINE_AA)
                #6
                p6 = np.array([[factorHeight*600,factorWidth*250],[factorHeight*708,factorWidth*248],[factorHeight*744,factorWidth*274],[factorHeight*636,factorWidth*279]],np.int32)
                cv.polylines(frame,[p6],True,(0,255,0) if stateFoot[2] == 6 and stateFoot[3] == 6 else (0,255,255),1, cv.LINE_AA)
                #7
                p7 = np.array([[factorHeight*209,factorWidth*228],[factorHeight*299,factorWidth* 220],[factorHeight*293,factorWidth*247],[factorHeight*197,factorWidth*248]],np.int32)
                cv.polylines(frame,[p7],True,(0,255,0) if stateFoot[2] == 7 and stateFoot[3] == 7 else (0,255,255),1, cv.LINE_AA)
                #8
                p8= np.array([[factorHeight*384,factorWidth*214],[factorHeight*459,factorWidth*212],[factorHeight*462,factorWidth*234],[factorHeight*388,factorWidth*243]],np.int32)
                cv.polylines(frame,[p8],True,(0,255,0) if stateFoot[2] == 8 and stateFoot[3] == 8 else (0,255,255),1, cv.LINE_AA)
                #9
                p9=np.array([[factorHeight*560,factorWidth*208],[factorHeight*643,factorWidth*197],[factorHeight*672,factorWidth*222],[factorHeight*582,factorWidth*226]],np.int32)
                cv.polylines(frame,[p9],True,(0,255,0) if stateFoot[2] == 9 and stateFoot[3] == 9 else (0,255,255),1, cv.LINE_AA)

                #plan_view = cv.warpPerspective(frame, H, (dst.shape[1], dst.shape[0]))
                
            
            frame = cv2.resize(frame, (frame.shape[1]*2,frame.shape[0]*2), interpolation = cv2.INTER_AREA)
            cv2.imshow("Frame", frame)
            if cv2.waitKey(1) & 0xFF == 27:
                break
    cap.release()
    cv2.destroyAllWindows()


main_(src_list,dst_list,"Fabian-30_fps.mp4")

