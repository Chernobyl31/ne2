from tkinter import *
import tkinter as tk
import time
from tkinter import filedialog
from tkinter import scrolledtext
from tkinter.messagebox import showinfo
from tkinter.scrolledtext import ScrolledText
from detectionwithSquaresFloor import *
from PIL import Image
from PIL import ImageTk
import imutils
import cv2

video = None
x = None
src = None
y = dst_list
def select_video():
    file_path = filedialog.askopenfilename(filetypes = [
        ("video", ".mp4")])
    global video
    video = file_path

def aceptar(datos):
    global x
    x = datos.get("1.0","end")
    x = x.rstrip("\n")

    try:
        string_to_array(x)
    except:
        popup_error()

def popup_showinfo():
    showinfo("Alerta", "Datos ingresados correctamente")

def popup_error():
    showinfo("Error", "Ocurrió un error al ingresar los datos")
    
def string_to_array(data):
    global src
    split_1 = data.split(';')
    split_2 = []
    if(len(split_1) == 6):        
        for comp in range(len(split_1)):
            split_2.append(split_1[comp].split(','))
        src = split_2
        for elem in range(len(split_2)):
            for sub_elem in range(len(split_2[elem])):
                split_2[elem][sub_elem] = float(split_2[elem][sub_elem])
        popup_showinfo()
    else:
        popup_error()
        
root = Tk()
root.title('Neuroentrenamiento 2')
root.resizable(width=False, height=False)
root.configure(bg='#46372F')

img= (Image.open("puntos.png"))

resized_image= img.resize((400,400), Image.ANTIALIAS)
new_image= ImageTk.PhotoImage(resized_image)

left_frame = Canvas(root, bg='#F773D0', highlightthickness=0)

puntos_hom = Label(left_frame, image=new_image, bd=0, highlightthickness=0)
right_frame = Canvas(root, bg='#714E33', highlightthickness=0)
right_frame.configure(bg="#714E33")

Datos = Label(right_frame, text = "Ingrese los datos para la homografia: ")
botonInicio = Button(right_frame, text='Iniciar Ejecución', bg="#69EADD",
                     command=lambda: main_(src,y, video))
Saludo = Canvas(left_frame,width=400,height=100, bg = "#F773D0", highlightthickness=0)
Saludo.create_text(195, 50, text = "Bienvenido a Neuroentrenamiento 2!",
                   fill="black", font=('Helvetica 15 bold'))
ingreso = scrolledtext.ScrolledText(right_frame, wrap = tk.WORD)
elegirVideo = Button(right_frame, text='Seleccionar Video', bg="#69EADD",
                     command= select_video)
aceptacion = Button(right_frame, text='Aceptar', bg="#69EADD",
                     command=lambda: aceptar(ingreso))

left_frame.grid(row=0, column=0) #FRAME IZQUIERDO
right_frame.grid(row=0, column=1)#FRAME DERECHO
#BIENVENIDA
Saludo.grid(row =0, column=0,columnspan=3, sticky="snwe")
#IMAGEN DE PUNTOS GUIA PARA HOMOGRAFIA
puntos_hom.grid(row=1,column=0, sticky="snwe")
#SELECCION VIDEO
elegirVideo.grid(row=0, column=0,columnspan=2, sticky = "snwe")
#INGRESO DE PUNTOS PARA HOMOGRAFIA
Datos.grid(row=2, column=0,columnspan=4, sticky = "snwe")
ingreso.grid(row=3, column=0, columnspan=4, sticky="snwe")
#EJECUCION DEL PROGRAMA
botonInicio.grid(row=0, column=2,columnspan=4, sticky = "snwe")
aceptacion.grid(row=4, column=0, columnspan=4, sticky="snwe")

root.mainloop()
